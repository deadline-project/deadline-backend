const task = require("../controller/task");
module.exports = (app) => {

    const task = require('../controller/task');

    app.get('/api/tasks', task.findAll);

    app.post('/api/addTask', task.create);

    app.post('/api/updateTask/:id', task.update);

    app.post('/api/deleteTask/:id', task.delete);

    app.get('/api/task/:id', task.findById);
};