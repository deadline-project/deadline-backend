var DataTypes = require("sequelize").DataTypes;
var _task = require("./task");
var _user = require("./user");
var _user_task = require("./user_task");

function initModels(sequelize) {
  var task = _task(sequelize, DataTypes);
  var user = _user(sequelize, DataTypes);
  var user_task = _user_task(sequelize, DataTypes);

  user_task.belongsTo(task, { as: "task", foreignKey: "task_id"});
  task.hasMany(user_task, { as: "user_tasks", foreignKey: "task_id"});
  user_task.belongsTo(user, { as: "user", foreignKey: "user_id"});
  user.hasMany(user_task, { as: "user_tasks", foreignKey: "user_id"});

  return {
    task,
    user,
    user_task,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
